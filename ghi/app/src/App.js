import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="home">
            <Route index element={<MainPage />} />
          </Route >
        </Routes>
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="attend">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
        </Routes>
        <Routes>
          <Route path="attendees">
            <Route path="new" element={<AttendeesList attendees={props.attendees} />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
